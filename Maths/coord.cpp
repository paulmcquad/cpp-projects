#include <iostream>
using namespace std;

class CoordPair
{
private:
    float x, y;

public:
    void GetUserInput()
    {
        cout << "Enter an x and y coordinate: ";
        cin >> x >> y;
        cout << " x is " << x << " y is " << y << endl;
    }
};

int main(int argc, char *argv[])
{
    CoordPair cor;

    cor.GetUserInput();

    return 0;
}