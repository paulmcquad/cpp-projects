#include <iostream>
#include <string>
using namespace std;

class Player
{
public:
    void Setup(const string &name, int hp)
    {
        this->name = name;
        this->hp = hp;
    }

    bool IsAlive()
    {
        return (hp > 0);
    }

    int GetHP()
    {
        return hp;
    }

    string GetName()
    {
        return name;
    }

    void SetAttackAndDefence(int attack, int defence)
    {
        this->attack = attack;
        this->defence = defence;
    }

    int GetAttack()
    {
        return attack;
    }

    void TakeDamage(int amount)
    {
        float damage = amount - defence;
        cout << name << " hit for " << damage << " of damage!" << endl;
        hp -= damage;
    }

private:
    string name;
    int hp;
    int attack;
    int defence;
};

int ChooseAction()
{
    int choice;
    cout << "1. Offensive attack" << endl;
    cout << "2. Defensive attack" << endl;
    cin >> choice;
    return choice;
}

int main(int argc, char *argv[])
{
    Player players[2];

    players[0].Setup("Player1", 100);
    players[1].Setup("Player2", 100);

    while (players->IsAlive())
    {
        cout << endl
             << "----------------------------------" << endl;
        for (int p = 0; p < 2; p++)
        {
            cout << players[p].GetName() << ": " << players[p].GetHP() << "\t";
        }
        cout << endl;

        for (int p = 0; p < 2; p++)
        {
            cout << players[p].GetName() << "'s turn" << endl;
            int choice = ChooseAction();

            if (choice == 1)
            {
                players[p].SetAttackAndDefence(10, 2);
            }
            else if (choice == 2)
            {
                players[p].SetAttackAndDefence(5, 5);
            }
        }
        players[0].TakeDamage(players[1].GetAttack());
        players[1].TakeDamage(players[0].GetAttack());
    }

    return 0;
}
