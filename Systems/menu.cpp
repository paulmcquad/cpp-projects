#include <iostream>
#include "menu.h"
using namespace std;

// Log in Menu
int Menu::LoginMenu() {
     cout << "LOG IN\n" << endl;

     cout << "0. QUIT" << endl;
     cout << "1. ADMIN"<< endl;
     cout << "2. EMPLOYEE" << endl;
     cout << ">>_";

     return 0;
}

// Employee Menu
int Menu::EmployeeMenu(){
     cout << "EMPLOYEE MENU\n" << endl;

     cout << "0. LOG OUT" << endl;

     cout << "MOVIES" << endl;
     cout << "1. " << movie1 << " (" << movie1seats << " SEATS)" << endl;
     cout << "2. " << movie2 << " (" << movie2seats <<  " SEATS)" << endl;
     cout << "3. " << movie3 << " (" << movie3seats <<  " SEATS)" << endl;
     cout << ">>_";

     return 0;
}

// Admin Menu
int Menu::AdminMenu(){
     cout << "ADMIN MENU\n" << endl;

     cout << "0. LOG OUT" << endl;
     cout << "1. SET TICKET PRICES" << endl;
     cout << "2. SET AUDITORIUM SEATS" << endl;
     cout << "3. SET MOVIE SHOWINGS" << endl;
     cout << ">>_";

     return 0;
}

// Print Current Ticket Prices
int Menu::AdminTicketPrices(){

     cout << "ADMIN MENU – SET TICKET PRICES\n" << endl;
     cout << "0. BACK "<< endl;
     cout << "1. CHILD TICKETS (€" << fixed << setprecision(2) << childticket << ")" << endl;
     cout << "2. ADULT TICKETS (€" << fixed << setprecision(2) << adultticket << ")" << endl;
     cout << "3. SENIOR TICKETS (€" << fixed << setprecision(2) << seniorticket << ")" << endl;
     cout << ">>";
    

     return 0;
}

// Print Changed Ticket Prices
int  Menu::AdminChangeTicketPrices(int choice){
 if (choice == 1){
          cin >> childticket;
          cout << "NEW CHILD TICKET PRICE: (€" << fixed << setprecision(2) << childticket << ")" << endl;
     }
     else if (choice == 2){
          cin >> adultticket;
          cout << "NEW ADULT TICKET PRICE: (€" << fixed << setprecision(2) << adultticket << ")" << endl;
     }
     else if (choice == 3){
          cin >> seniorticket;
          cout << "NEW SENIOR TICKET PRICE: (€" << fixed << setprecision(2) << seniorticket << ")" << endl;

     }
     return 0;
}




// Book Tickets
int Menu::EmployeeSelectTickets(){

     cout << "HOW MANY CHILD TICKETS - (€" << fixed << setprecision(2) << childticket << ")? ";
     cin >> amount;
     subtotal1 = childticket * amount;
     
     sub1seats = (movie1seats - amount);
     cout << "HOW MANY ADULT TICKETS - (€" << fixed << setprecision(2) << adultticket << ")? ";
     cin >> amount;
     subtotal2 = adultticket * amount;
     
     sub2seats = (movie1seats - amount);
     cout << "HOW MANY SENIOR TICKETS - (€" << fixed << setprecision(2) << seniorticket << ")? ";
     cin >> amount;
     subtotal3 = seniorticket * amount;
     
     sub3seats = (movie1seats - amount);
     
     totalseats = (sub1seats - sub2seats - sub3seats);
     cout << "\nTotal Seats: (" << (totalseats) << ") ";

     //cout << "\nTOTAL: (€"<< fixed << setprecision(2) << (subtotal1 + subtotal2 + subtotal3) << ") ";
     //sub3seats = movie1seats - amount;
     
     //total = sub1seats + sub2seats + sub3seats;
     // if (total <= 10)
     //      cout << "\nTOTAL: (€"<< fixed << setprecision(2) << (subtotal1 + subtotal2 + subtotal3) << ") ";
     // else
     //      cout << "Not more seats available ";
     return 0;
}

// Print Current Auditorium Seats
int Menu::AuditoriumSeats(){

    cout << "ADMIN MENU –  SET AUDITORIUM SEATS\n" << endl;
    cout << "1. " << movie1 << " (" << movie1seats << " SEATS)" << endl;
    cout << "2. " << movie2 << " (" << movie2seats <<  " SEATS)" << endl;
    cout << "3. " << movie3 << " (" << movie3seats <<  " SEATS)" << endl;
    cout << ">>_";

    return 0;
}

// Print Changed Auditorium Seats
int Menu::AuditoriumChangeSeats(int choice){
    if (choice == 1){
          cin >> movie1seats;
                cout << "1. " << movie1 << " (" << movie1seats << " SEATS)" << endl;
     }
     else if (choice == 2){
          cin >> movie2seats;
                cout << "2. " << movie2 << " (" << movie2seats <<  " SEATS)" << endl;
     }
     else if (choice == 3){
          cin >> movie3seats;
                cout << "3. " << movie3 << " (" << movie3seats <<  " SEATS)" << endl;
     }
     return 0;
}

// Print Changed Movie Showings
int Menu::MovieShowings(int choice){
    if (choice == 1){
          cin.ignore();
          getline ( cin, movie1 );
          cout << "1. " << movie1 << " (" << movie1seats << " SEATS)" << endl;
     }
     else if (choice == 2){
          cin.ignore();
          getline ( cin, movie2 );
                cout << "2. " << movie2 << " (" << movie2seats <<  " SEATS)" << endl;
     }
     else if (choice == 3){
          cin.ignore();
          getline ( cin, movie3 );
                cout << "3. " << movie3 << " (" << movie3seats <<  " SEATS)" << endl;
     }
     return 0;
}

