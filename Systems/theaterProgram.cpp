#include <iostream>
#include "menu.h"

using namespace std;


int main(int argc, char *argv[])
{
    Menu menu;
    bool programDone = false;
    int choice = 0;

    while (!programDone){

        menu.LoginMenu(); // Log in Menu

        cin >> choice;  //Get choice
        if (choice == 0)
        {
            programDone = true;
        }
        else if (choice == 1)
        {
            bool adminDone = false;
            while (!adminDone)
            {
                menu.AdminMenu(); // Admin Menu
                cin >> choice;  //Get choice
                if (choice == 0)
                {
                     adminDone = true; // Return to Login Menu
                }
                else if(choice == 1)
                {
                    menu.AdminTicketPrices(); // Print Current Ticket Prices
                    cin >> choice;  //Get choice
                    menu.AdminChangeTicketPrices(choice); // Print Current Ticket Prices
                }
                else if(choice == 2)
                {
                    menu.AuditoriumSeats(); // Print Current Auditorium Seats
                    cin >> choice;
                    menu.AuditoriumChangeSeats(choice); // Print Changed Auditorium Seats
                }
                if (choice == 3)
                {
                    menu.EmployeeMenu(); // Employee Menu
                    cin >> choice;
                    menu.MovieShowings(choice);   // Print Changed Movie Showings
                }
            }
        }
        else if (choice == 2)
        {
            bool employeeDone = false;
            while (!employeeDone)
            {
                menu.EmployeeMenu(); // Employee Menu
                cin >> choice;  //Get choice
                if (choice == 0)
                {
                     employeeDone = true; // Return to Login Menu
                }
                else if(choice == 1) // Movie 1 - MY NEIGHBOR TOTORO (10 SEATS)
                {
                    cout <<  menu.movie1 << "(" << menu.movie1seats << " SEATS)\n" << endl;
                    menu.EmployeeSelectTickets();  // Book Tickets
                    cin >> choice;  //Get choice
                }
                else if(choice == 2) // Movie 2 - COMING TO AMERICA (15 SEATS)
                {
                    cout << menu.movie2 << "(" << menu.movie2seats << " SEATS)\n" << endl;
                    menu.EmployeeSelectTickets();  // Book Tickets
                    cin >> choice;  //Get choice
                }
                if (choice == 3) // Movie 3 - BEETLE JUICE (12 SEATS)
                {
                    cout << menu.movie3 << "(" << menu.movie3seats << " SEATS)\n" << endl;
                    menu.EmployeeSelectTickets();  // Book Tickets
                    cin >> choice;  //Get choice
                }
            }
        }
    }
    return 0;
}