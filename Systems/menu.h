#ifndef MENU_H
#define MENU_H
#include <iostream>
#include <string>
#include <iomanip>
using namespace std;
class Menu
{
private:
    double subtotal1 = 0;
    double subtotal2 = 0;
    double subtotal3 = 0;

    int sub1seats = 0;
    int sub2seats = 0;
    int sub3seats = 0;
    int totalseats = 0;

    int amount = 0;

public:
    string movie1 = "MY NEIGHBOR TOTORO";
    string movie2 = "COMING TO AMERICA";
    string movie3 = "BEETLE JUICE";

    int movie1seats = 10; // My Neighbor Totoro
    int movie2seats = 15; // Coming to America
    int movie3seats = 12; // Beetle Juice

    double childticket = 3.50;
    double adultticket = 5.50;
    double seniorticket = 4.50;

    int choice = 0; // Ticket Selection

    int LoginMenu();                                // Log in Menu
    int EmployeeMenu();                             // Employee Menu
    int AdminMenu();                                // Admin Menu
    
    int AdminTicketPrices();                        // Print Current Ticket Prices
    int AdminChangeTicketPrices(int choice);        // Print Changed Ticket Prices
    int AuditoriumSeats();                          // Print Current Auditorium Seats
    int AuditoriumChangeSeats(int choice);          // Print Changed Auditorium Seats
    int MovieShowings(int choice);                            // Print Changed Movie Showings

    int EmployeeSelectTickets();                    // Book Tickets

};

#endif // MENU_H