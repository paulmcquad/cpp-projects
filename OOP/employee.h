#ifndef EMPLOYEE_H
#define EMPLOYEE_H
#include <string>
using namespace std;

class AbstractEmployee {                // Abstraction
   virtual void AskForPromotion()=0;    // Pure virtual function
};

class Employee: AbstractEmployee        // Added 
{
protected:
    string name;    // My Data - Protected
    string company;
    int age; 
private:

public:
    void Intro();

    // Setters
    void setName(string name);
    void setCompany(string company);
    void setAge(int age);

    // Getters
    string getName();
    string getCompany();
    int getAge();

    Employee(string name, string company, int age); // My Constructor
    void AskForPromotion(); // Pure virtual function Implement (child)
    virtual void Work() // Pure virtual function (use child instead)
    {
        cout << name <<" is checking email, task backlog, performing tasks..." << endl;
    }
};

#endif // EMPLOYEE_H