# Object Oriented Programming (OOP) in C++ Course

```
* This course was developed by Saldina Nurak (CodeBeauty)
* Check out her channel: https://www.youtube.com/c/CodeBeauty
```

[Object Oriented Programming (OOP) in C++ Course](https://www.youtube.com/watch?v=wN0x9eZLix4&t=412s)

# Building Project on Windows

 Building on Windows with CMake and MinGW
 Run on (cmd)
```
 mkdir build
 cd build
 cmake .. -G "MinGW Makefiles"
 mingw32-make
```
# Building Project on Linux / Unix

 Building on Linux with CMake and g++.
 Run on (Konsole)
```
 mkdir build
 cd build
 cmake ..
 make
```