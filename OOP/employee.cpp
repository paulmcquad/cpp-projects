#include <iostream>
#include "employee.h"
using namespace std;

 // My Constructor
Employee::Employee(string name, string company, int age){
        this->name = name;
        this->company = company;
        this->age = age;
}

// My Functions
void Employee::Intro(){
        cout << "Name - " << name << endl;
        cout << "Company - " << company << endl;
        cout << "Age - " << age << endl;

}

void Employee::AskForPromotion(){   // Pure virtual function Implement

        if (age > 30)
            cout << name << " got promoted!" << endl;
        else
            cout << name << ", sorry No promotion for you!" << endl;
}

// Setters
void Employee::setName(string name){
    this->name = name;
}

void Employee::setCompany(string company){
    this->company = company;
}

void Employee::setAge(int age){
    if(age >= 18)
    this->age = age;
}

// Getters
string Employee::getName(){
    return name;
}

string Employee::getCompany(){
    return company;
}

int Employee::getAge(){
    return age;
}