#ifndef DEVELOPER_H
#define DEVELOPER_H
#include <string>
#include <iostream>
#include "employee.h"

using namespace std;

//  Child : Parent Class
class Developer : public Employee { // Made Inheritance Public
    public:
        string FavProgrammingLanguage;

        Developer(string name, string company, int age, string FavProgrammingLanguage)
            :Employee(name, company, age)
        {
            this->FavProgrammingLanguage = FavProgrammingLanguage;
        }

        void FixBug(){
            cout << name << " fixed bug using " << FavProgrammingLanguage << endl;
        }
        void Work()
        {
            cout << name <<" is writing " << FavProgrammingLanguage <<  " code "<< endl;
        }
};

#endif // DEVELOPER_H
