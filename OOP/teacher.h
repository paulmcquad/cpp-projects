#ifndef TEACHER_H
#define TEACHER_H
#include <string>
#include <iostream>
#include "employee.h"

using namespace std;

//  Child : Parent Class
class Teacher: public Employee {
public:
    string subject;
    void PrepareLesson(){
        cout << name << " is preparing " << subject << " lesson" <<   endl;
    }
    
    void Work()
    {
        cout << name <<" is teaching " << subject << endl;
    }

    // My Constructor
    Teacher(string name, string company, int age, string subject)
        :Employee(name, company, age)
        {
            this->subject = subject;
        }
};

#endif // TEACHER_H