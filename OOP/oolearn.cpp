#include <iostream>
#include "employee.h"
#include "developer.h"
#include "teacher.h"

int main(int argc, char *argv[])
{

    //Employee employee1 = Employee("Paul", "Coderforge", 34); // Call Constructor
    //Employee employee2 = Employee("Donna", "Linux Foundation", 26); // Call Constructor

    // The most common use of ploymorphism is when a
    // Parent class reference is used to a child class object

    Developer d = Developer("Paul", "Linux Foundation", 26, "C++");
    Teacher t = Teacher("Jack", "My Rock School", 35, "English");

    Employee *e1 = &d;
    Employee *e2 = &t;

    e1->Work();
    e2->Work();

    return 0;
}